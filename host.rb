#!/usr/bin/env ruby

require 'sinatra'
require 'kramdown'
require 'mustache'

get '/' do
  Mustache.template_file = 'template.mustache'
  view = Mustache.new
  view[:articles] = Dir.entries('out').select {|e| !e.match /^\./ }.map {|e|
    e.gsub /^(.*)\..+$/, '\1'
  }.sort
  view.render
end

get '/article/:page' do
  Mustache.template_file = 'template.mustache'
  view = Mustache.new
  view[:content] = Kramdown::Document.new(
    File.open("out/#{params[:page]}.md", 'r').read
  ).to_html
  view.render
end
