#!/usr/bin/env ruby

require 'rubygems'
require 'readability'
require 'open-uri'
require './reverse_markdown'

pages = (ARGV[2] || 1).to_i
name = ARGV[1] || 'readable'

source_html = "<p><a href=\"#{ARGV[0]}\">Original Article</a></p>"

readable = [source_html].concat((1..pages).map do |n|
  url = ARGV[0]
  url = "#{url}?page=#{n}" unless 1 == n
  source = open(url).read
  Readability::Document.new(
    source,
    :tags => %w[h1 h2 div p img a],
    :attributes => %w[src href]).content
end).join("\n\n")

r = ReverseMarkdown.new
markdown = r.parse_string readable

Dir.mkdir 'out' unless File.exists? 'out'

destroy_me = {
  /^To read the new issue .*? Click Here$/ => '',
  /(^ +| +$)/ => '',
  /\n\n+/ => "\n\n"
}

destroy_me.each do |regex, replacement|
  markdown = markdown.gsub regex, replacement
end

# File.open("out/#{name}.html", 'w') do |f|
#   f.write readable
# end

File.open("out/#{name}.md", 'w') do |f|
  f.write markdown.strip + "\n"
end
