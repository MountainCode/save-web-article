Save Articles to Markdown
=========================

Allows you to save web articles locally to markdown and host them from Sinatra.

Uses the [readability gem](https://github.com/cantino/ruby-readability) to parse articles from webpages.

Usage
-----

run

```bash
$ ./go.rb url output_file_name number_of_pages
```

serve

```bash
$ ./host.rb
```
